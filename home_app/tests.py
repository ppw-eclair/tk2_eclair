from django.test import TestCase, Client
from django.urls import resolve
from home_app.views import homeView, register_view, login_view, logout_view
from django.contrib.auth.models import User

# Create your tests here.
class TestHomeApp(TestCase):
	def test_url_homeapp(self):
		c = Client()
		response = c.get('/')
		self.assertEqual(response.status_code, 200)
	
	def test_url_salah(self):
		c = Client()
		response = c.get('/next')
		self.assertEqual(response.status_code, 404)

	def test_view_function(self):
		response = resolve('/')
		self.assertEqual(response.func, homeView)

	def test_use_template(self):
		c = Client()
		response = c.get('/')
		self.assertTemplateUsed(response, 'beranda.html')

class AuthenticationUnitTest(TestCase):
    def test_url_register(self):
        c = Client()
        response = c.get('/register/')
        self.assertEqual(response.status_code, 200)
    
    def test_url_login(self):
        c = Client()
        response = c.get('/login/')
        self.assertEqual(response.status_code, 200)

    def test_url_logout(self):
        c = Client()
        response = c.get('/logout/')
        self.assertEqual(response.status_code, 302)

    def test_view_register(self):
        response = resolve('/register/')
        self.assertEqual(response.func, register_view)

    def test_view_login(self):
        response = resolve('/login/')
        self.assertEqual(response.func, login_view)

    def test_view_logout(self):
        response = resolve('/logout/')
        self.assertEqual(response.func, logout_view)

    def test_use_template_register(self):
        c = Client()
        response = c.get('/register/')
        self.assertTemplateUsed(response, 'register.html')

    def test_use_template_login(self):
        c = Client()
        response = c.get('/login/')
        self.assertTemplateUsed(response, 'login.html')

    def setUp(self):
        user = User.objects.create_user(username="tester1",password="ing1nLulus")
        user.save()

    def test_register(self):
        response = self.client.post('/register/',data={
            'username':"pengenLulus",
            'first_name':"Saya", 
            'last_name':"Ingin", 
            'email':"cepatlulus@gmail.com",
            'password1':'amin1290',
            'password2': 'amin1290',
        })
        user = User.objects.all()
        self.assertEqual(len(user),2)
        self.assertIn('_auth_user_id',self.client.session)
        self.assertIn('_auth_user_backend', self.client.session)
        self.assertIn('_auth_user_hash', self.client.session)
        self.assertEqual(response.status_code,302)
        self.assertRedirects(expected_url='/',response=response)

    def test_login(self):
        response = self.client.post('/login/',data={
            'username':"tester1",
            'password':'ing1nLulus'
        })

        self.assertIn('_auth_user_id',self.client.session)
        self.assertIn('_auth_user_backend', self.client.session)
        self.assertIn('_auth_user_hash', self.client.session)
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(expected_url='/',response=response)

    def test_logout(self):
        response = self.client.post('/login/',data={
            'username':"tester1",
            'password':'ing1nLulus'
        })

        response = self.client.post('/logout/')
        self.assertNotIn('_auth_user_id',self.client.session)
        self.assertNotIn('_auth_user_backend', self.client.session)
        self.assertNotIn('_auth_user_hash', self.client.session)
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(expected_url='/',response=response)