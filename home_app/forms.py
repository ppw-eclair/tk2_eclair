from django import forms
from django.forms.widgets import PasswordInput
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth.models import User

class RegisterForm(UserCreationForm):
    first_name = forms.CharField(max_length=30, required=False, help_text='Please enter your name')
    last_name = forms.CharField(max_length=30, required=False, help_text='Please enter your name')
    email = forms.EmailField(max_length=254, help_text='Please type your email address')

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email', 'password1', 'password2', )

class LoginForm(AuthenticationForm):
    username = forms.CharField(max_length=30, required=True, help_text='Please enter your username')
    password = forms.CharField(max_length=30, required=True, help_text='Please enter your password', widget=PasswordInput)