from django.urls import path
from . import views

app_name = "home_app"
urlpatterns = [
    path('', views.homeView, name="beranda"),
    path('register/', views.register_view, name="register"),
    path('login/', views.login_view, name="login"),
    path('logout/', views.logout_view, name='logout'),
]