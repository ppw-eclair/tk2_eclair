from django.test import TestCase, Client
import unittest
from django.urls import resolve
from .views import faq_view
from .models import Pertanyaan

# Create your tests here.
class FAQ(TestCase)  :
	def test_url_faq(self):
		c = Client()
		response = c.get('/faq/')
		self.assertEqual(response.status_code, 200)

	def test_url_salah(self):
		c = Client()
		response = c.get('/salah')
		self.assertEqual(response.status_code, 404)

	def test_template(self):
		c = Client()
		response = self.client.get('/faq/')
		self.assertTemplateUsed(response, 'faq.html')

	def test_view_faq(self):
		c = Client()
		response = resolve('/faq/')
		self.assertEqual(response.func, faq_view)

	def test_database(self):
		pertanyaan = Pertanyaan(pertanyaan = 'ini untuk coba database bagian pertanyaan')
		pertanyaan.save()
		hitung_jumlah = Pertanyaan.objects.all().count()
		self.assertEqual(hitung_jumlah, 1)


