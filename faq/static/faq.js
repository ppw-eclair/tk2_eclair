//jscript untuk loading
$(window).load(function() {
  $("#loading").hide();
});


//jscript untuk accordion
$(function() {
  var acc = $(".accordion");
  var i;

  for (i = 0; i < acc.length; i++) {
    acc[i].addEventListener("click", function() {
      this.classList.toggle("active");

      var panel = this.nextElementSibling;
      if (panel.style.display === "block") {
        panel.style.display = "none";
      } else {
        panel.style.display = "block";
      }
    });
  }
});

//jscript untuk cari pertanyaan didalam accordion
function myFunction() {
  // Declare variables
  console.log("coba ini masuk ga?");
  var input, filter, ul, li, p, i, txtValue;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  ul = document.getElementById("myUL");
  li = ul.getElementsByTagName("li");

  // Loop through all list items, and hide those who don't match the search query
  for (i = 0; i < li.length; i++) {
    p = li[i].getElementsByTagName("p")[0];
    txtValue = p.textContent || p.innerText;
    if (txtValue.toUpperCase().indexOf(filter) > -1) {
      li[i].style.display = "";
    } else {
      li[i].style.display = "none";
    }
  }
}

function scrollFunction() {
  if (document.body.scrollTop > 10 || document.documentElement.scrollTop > 10)
    document.getElementById("myBtn").style.display = "block";
  else document.getElementById("myBtn").style.display = "none";
}

//jscript untuk scroll ke atas
$(window).scroll(function() {
  scrollFunction();
});

// When the user clicks on the button, scroll to the top of the document
function topFunction() {
  window.scrollTo(0, 0);
}

