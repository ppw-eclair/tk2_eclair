from django.shortcuts import render, redirect
from .forms import question
from .models import Pertanyaan
from django.core import serializers
from django.shortcuts import HttpResponse

# Create your views here.
def faq_view(request):
	if request.method == 'POST':
		form = question(request.POST)
		if form.is_valid():
			form.save()
			return redirect ('faq:faq')
	else:
		form = question()
		semua_pertanyaan = Pertanyaan.objects.all()
		return render(request, 'faq.html', {'form':form, 'semua_pertanyaan':semua_pertanyaan})
		
def webservice(request):
	data = serializers.serialize("json", Pertanyaan.objects.all())
	return HttpResponse(data, content_type = 'application/json')