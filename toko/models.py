from django.db import models

# Create your models here.
class TokoIkan(models.Model):
    foto=models.ImageField(upload_to='fotoku/', default="")
    nama=models.CharField(max_length=100)
    nomorHp=models.CharField(max_length=14)
    email=models.EmailField()

    def __str__(self):
        return self.nama
