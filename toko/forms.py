from django import forms
from . import models

class BuatToko(forms.Form):
    foto=forms.ImageField(required=True)
    nama=forms.CharField(label='nama', 
                            max_length=100, 
                            required=True, 
                            widget=forms.TextInput(attrs={'placeholder': 'Toko Sumber Ikan', 'class':'form-control'})
                            )

    nomor=forms.CharField(label='nomor', 
                            required=True, 
                            widget=forms.NumberInput(attrs={'placeholder': '087812344432', 'class':'form-control'})
                            )

    email=forms.EmailField(label='email', 
                            max_length=100, 
                            required=True, 
                            widget=forms.TextInput(attrs={'placeholder': 'sumberikan@gmail.com', 'class':'form-control'})
                            )