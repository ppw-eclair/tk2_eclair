from django.core.files.uploadedfile import SimpleUploadedFile
from django.test import TestCase
from django.test import Client
from django.urls import resolve

from .views import *
from .models import TokoIkan

class TokoTest(TestCase):
    def test_toko_url_is_exist(self):
        response = Client().get('/toko/')
        self.assertEqual(response.status_code, 200)

    def test_toko_fungsi(self):
        found = resolve('/toko/')
        self.assertEqual(found.func, displayToko)

    def test_toko_url_use_template(self):
        response = Client().get('/toko/')
        self.assertTemplateUsed(response, 'displayToko.html')
    
    def test_buat_toko_url_is_exist(self):
        response = Client().get('/toko/tambah/')
        self.assertEqual(response.status_code, 200)

    def test_buat_toko_fungsi(self):
        found = resolve('/toko/tambah/')
        self.assertEqual(found.func, tambahToko)

    def test_buat_toko_url_make_template(self):
        response = Client().get('/toko/tambah/')
        self.assertTemplateUsed(response, 'tambahToko.html')

    def test_models_buat_toko_ikan(self):
        small_gif = (
            b'\x47\x49\x46\x38\x39\x61\x01\x00\x01\x00\x00\x00\x00\x21\xf9\x04'
            b'\x01\x0a\x00\x01\x00\x2c\x00\x00\x00\x00\x01\x00\x01\x00\x00\x02'
            b'\x02\x4c\x01\x00\x3b'
        )
        uploaded = SimpleUploadedFile('foto.gif', small_gif, content_type='fotoku/gif')

        TokoIkanTest = TokoIkan(
            foto = uploaded,
            nama = "Toko Ikan Sehat",
            nomorHp="087882255409",
            email = "ikansehat@gmail.com",
            )
        TokoIkanTest.save()

        semua_tokoIkan = TokoIkan.objects.all()
        self.assertEqual(len(semua_tokoIkan), 1)
        self.assertEqual(semua_tokoIkan[0].foto, 'fotoku/foto.gif')
        self.assertEqual(semua_tokoIkan[0].nama, "Toko Ikan Sehat")
        self.assertEqual(semua_tokoIkan[0].nomorHp, "087882255409")
        self.assertEqual(semua_tokoIkan[0].email, "ikansehat@gmail.com")

    def test_string_model_toko_ikan(self):
        tokoIkan = TokoIkan.objects.create(nama="MAKAN")
        self.assertEqual(str(tokoIkan), "MAKAN")
