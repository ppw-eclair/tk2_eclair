from django.shortcuts import render, HttpResponse, render, redirect
from .models import TokoIkan
from .forms import BuatToko
from django.core import serializers
import json

# Create your views here.
def displayToko(req):
    daftarToko = TokoIkan.objects.order_by()
    return render(req, 'displayToko.html', {'daftarToko':daftarToko})

def tambahToko(req):
    form = BuatToko()
    return render(req, 'tambahToko.html', {'form':form})

def buat(request):
    form=BuatToko(request.POST, request.FILES)
    if(request.method=='POST' and form.is_valid()):
        toko=TokoIkan(
                foto=request.FILES["foto"],
                nama=form.data["nama"],
                nomorHp=form.data["nomor"],
                email=form.data["email"],
            )
        toko.save()
    return redirect('/toko/')

def hapusTok(request):
    if request.method == "POST" and 'id' in request.POST :
        id = request.POST['id']
        TokoIkan.objects.get(id=id).delete()
    return redirect('/toko/')

def returnJson(request):
    daftarToko = TokoIkan.objects.order_by()
    jsonToko = serializers.serialize("json", daftarToko)
    data = json.loads(jsonToko)
    balikin = json.dumps(data)
    return HttpResponse(balikin, content_type="application/json")
