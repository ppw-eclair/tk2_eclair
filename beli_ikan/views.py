from django.shortcuts import render, redirect
from django.http import HttpResponse
from . import forms
from .models import Ikan
from django.core import serializers

def buat_ikan_view(request):
    if request.method == 'POST':
        form = forms.BuatIkan(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return redirect ('beli_ikan:form_ikan')
    else:
        form = forms.BuatIkan()
        return render(request, 'buat_ikan.html', {'form_ikan':form})

def daftar_ikan_view(request):
    if request.method=='POST':
        form = forms.BuatIkan(request.POST)
        if form.is_valid():
            form.save()
        
    semua_ikan = Ikan.objects.all().order_by('jenis','harga')
    return render(request, "daftar_ikan.html", {'semua_ikan':semua_ikan})

def ambil_data_json(request):
    semua_ikan = Ikan.objects.all().order_by('jenis','harga')
    list_ikan = serializers.serialize('json', semua_ikan)
    return HttpResponse(list_ikan, content_type="application/json")

def hapus_Ikan(request, id):
    ikan_hapus = Ikan.objects.get(id=id)
    ikan_hapus.delete()
    return redirect('beli_ikan:form_ikan')