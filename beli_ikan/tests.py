from django.test import TestCase, Client
from django.urls import resolve
from beli_ikan.views import daftar_ikan_view, buat_ikan_view
from beli_ikan.models import Ikan
from django.core.files.uploadedfile import SimpleUploadedFile

# Create your tests here.
class TestBeliIkanApp(TestCase):
	def test_url_daftar_ikan(self):
		c = Client()
		response = c.get('/beli/')
		self.assertEqual(response.status_code, 200)
	
	def test_url_salah(self):
		c = Client()
		response = c.get('/next')
		self.assertEqual(response.status_code, 404)

	def test_view_daftar_ikan(self):
		response = resolve('/beli/')
		self.assertEqual(response.func, daftar_ikan_view)

	def test_use_template(self):
		c = Client()
		response = c.get('/beli/')
		self.assertTemplateUsed(response, 'daftar_ikan.html')

class TestBeliIkanApp2(TestCase):
    def test_view_buat_ikan(self):
        response = resolve('/beli/terima')
        self.assertEqual(response.func, buat_ikan_view)

    def test_url_beli_terima(self):
        c = Client()
        response = c.get('/beli/terima')
        self.assertEqual(response.status_code, 200)

    def test_template_form(self):
        c = Client()
        response = c.get('/beli/terima')
        self.assertTemplateUsed(response, 'buat_ikan.html')
    
    def test_buat_ikan(self):
        small_gif = (
            b'\x47\x49\x46\x38\x39\x61\x01\x00\x01\x00\x00\x00\x00\x21\xf9\x04'
            b'\x01\x0a\x00\x01\x00\x2c\x00\x00\x00\x00\x01\x00\x01\x00\x00\x02'
            b'\x02\x4c\x01\x00\x3b'
        )
        uploaded = SimpleUploadedFile('small.gif', small_gif, content_type='image/gif')

        ikanTest = Ikan(
            id = 1,
            jenis="Test",
            foto = uploaded,
            harga = 100000,
            deskripsi = "Deskripsi",
            )
        ikanTest.save()

        semua_ikan = Ikan.objects.all()
        self.assertEqual(len(semua_ikan), 1)
        self.assertEqual(semua_ikan[0].jenis, "Test")
        self.assertEqual(semua_ikan[0].foto, 'images/small.gif')
        self.assertEqual(semua_ikan[0].harga, 100000)
        self.assertEqual(semua_ikan[0].deskripsi, "Deskripsi")

    def test_def_string_models(self):
        jenis_test = Ikan.objects.create(jenis="Testing")
        self.assertEqual(str(jenis_test),"Testing")