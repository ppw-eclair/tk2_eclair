$(document).ready(() =>{
    $( ".card" ).hover(function() {
        $(this).addClass('shadow-lg').css('cursor', 'pointer'); 
    }, function() {
        $(this).removeClass('shadow-lg');
    }
    );
        
    $('.btn_hapus_ikan').click(function() {
        if(confirm("Yakin ingin menghapus ikan?")){
            return true;
        }else{
            return false;
        }
    });
});

$(window).load(function() {
    $('#loading').hide();
});