from django.urls import path
from . import views

app_name = "beli_ikan"
urlpatterns = [
    path('', views.daftar_ikan_view, name="form_ikan"),
    path('terima', views.buat_ikan_view, name="terima"),
    path('delete/<id>', views.hapus_Ikan, name="hapus"),
    path('datajson', views.ambil_data_json, name="ambil_data"),
]