$(window).load(function() {
  // Animate loader off screen
  $(".se-pre-con").fadeOut("slow");
});



$(document).ready(function(){
  function searchBlog(toSearch){
    $.ajax({
      url: '/webservice/',
      success: function (results){
        $('#myTable').html("");
        for (var i = 0; i < results.length; ++i) {
          var result = results[i];
          if (result.fields.description.include(toSearch)||result.fields.judul.include(toSearch)) {
            print();
              $("#myTable").append(
                '<div class="row no-gutters">'+
                      '<div class="col-md-4">' +
                         ' <img src='+result.fields.foto + 'class="card-img foto-blog" alt="image">' +
                      '</div>' +
                      '<div class="col-md-8">' +
                          '<div class="card-body persegmen">' +
                             ' <h5 class="card-title judul">' + result.fields.judul +'</h5>' +
                              '<p class="card-text deskripsi">' + result.fields.deskripsi + '</p>' +
                          '</div></div></div>'
              )
          }
      }
      }
    })
  }
  
    $('.persegmen').hover(function(){
      $(this).css("background-color", "#FFFFFF");
      }, function(){
      $(this).css("background-color", "#D2E7FF");
    });

    $('#btn-tambahBlog').click(function(){
      if(confirm("Yakin mau menambahkan blog ini?")){
        return true;
      }
      return false;
    })


})

