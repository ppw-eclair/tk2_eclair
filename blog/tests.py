from django.test import TestCase, Client
from blog.views import buat_blog, blog_view, blog_delete
from blog.models import BlogModels
from django.urls import resolve
from django.core.files.base import File
from django.http import HttpRequest
from django.core.files.uploadedfile import SimpleUploadedFile

# Create your tests here.
class unitTest(TestCase):
    def test_url_exist(self):
        response = self.client.get('/blog/')
        self.assertEqual(response.status_code, 200)

    def test_url_to_html(self):
        response = self.client.get('/blog/')
        self.assertTemplateUsed(response, 'view_blog.html')
    
    def test_url_not_exist(self):
        response = Client().get('/hai')
        self.assertEqual(response.status_code, 404) 

    def test_model_str(self):
        test_model = BlogModels(judul = "Aku", deskripsi = "test aja", id=1)
        test_model.save()
        test_model = BlogModels.objects.get(judul="Aku")
        stringnya = str(test_model)
        self.assertEqual("Aku", stringnya)

    def test_View_function_used_buat_blog(self):
        response = resolve('/blog/buat')
        self.assertEqual(response.func, buat_blog)
    def test_view_succeeded(self):
        small_gif = (
            b'\x47\x49\x46\x38\x39\x61\x01\x00\x01\x00\x00\x00\x00\x21\xf9\x04'
            b'\x01\x0a\x00\x01\x00\x2c\x00\x00\x00\x00\x01\x00\x01\x00\x00\x02'
            b'\x02\x4c\x01\x00\x3b'
        )
        
        uploaded = SimpleUploadedFile('small.gif', small_gif, content_type='image/gif')
        request = {
            "judul":"Hai",
            "deskripsi":"asdasdasdasdasd",
            "foto": uploaded
        }

        blog_creation = BlogModels(
            judul="Hai",
            deskripsi="asdasdasdasdasdasdsad",
            foto = uploaded
        )
        blog_creation.save()

        response = self.client.post("/blog/buat/",data=request)
        objects = BlogModels.objects.filter(id=1)
        self.assertEqual(len(objects),1)
    def test_view_delete(self):
        request = {
            "judul":"Hai",
            "deskripsi":"asdasdasdasdasd"
        }

        small_gif = (
            b'\x47\x49\x46\x38\x39\x61\x01\x00\x01\x00\x00\x00\x00\x21\xf9\x04'
            b'\x01\x0a\x00\x01\x00\x2c\x00\x00\x00\x00\x01\x00\x01\x00\x00\x02'
            b'\x02\x4c\x01\x00\x3b'
        )
        uploaded = SimpleUploadedFile('small.gif', small_gif, content_type='image/gif')

        blog_creation = BlogModels(
            judul="Hai",
            deskripsi="asdasdasdasdasdasdsad",
        )
        blog_creation.save()





# Create your tests here.
