# TK2_Kel_Eclair

Tugas Kelompok 2 - Perancangan dan Pemrograman Web 2019 - Batch of Quanta 2018

[![pipeline status](https://gitlab.com/ppw-eclair/tk2_eclair/badges/master/pipeline.svg)](https://gitlab.com/ppw-eclair/tk2_eclair/commits/master)
[![coverage report](https://gitlab.com/ppw-eclair/tk2_eclair/badges/master/coverage.svg)](https://gitlab.com/ppw-eclair/tk2_eclair/commits/master)

Link Heroku app:

[Eclair Lakes](http://eclair-lakes.herokuapp.com)

Sebuah entitas kelompok yang beranggotakan:
1. Evando Wihalim - 1806205445
2. Felicia Honggo - 1806190872
3. Muhammad Mudrik - 1806190935
4. Nasywa Nur Fathiyah - 1806205546

Ide:
Didasari keinginan untuk membantu nelayan dalam menjangkau lebih banyak pembeli dan membantu pembeli dalam mendapatkan ikan paling segar dengan cara yang ringkas, kelompok kami membuat sebuah website bernama Éclair-lake. Website Eclair-lake adalah sebuah website yang berfungsi untuk menghubungkan pembeli dengan nelayan. Terdapat berbagai macam peran yang mungkin mengunjungi website kami, namun peran yang kami utamakan adalah nelayan, pembeli, dan pengunjung pada umumnya.

Fitur:
1. Melihat berbagai jenis toko yang menjual ikan (dalam hal ini nelayan)
2. Melihat berbagai macam ikan untuk dibeli
3. Melihat informasi menarik seputar ikan
4. Melihat pertanyaan yang paling sering ditanyakan